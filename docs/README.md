# Terminal Typeracer documentation

## Here you'll find information on

* Building Typeracer from source
* Using the Typeracer config file to customize your Typeracer
* Information on the lang_pack format if you'd like to use your own packs
